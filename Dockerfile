ARG BASE_IMAGE="alpine:3.15"
ARG SRC_IMAGE="kinsamanka/tango-on-alpine:0.1"

FROM $SRC_IMAGE as src

ARG PYTANGO_VER=9.3.3

USER root
RUN apk --update add --no-cache boost-dev g++ libsodium-dev pkgconfig py3-numpy py3-pip py3-wheel python3-dev && \
    pip wheel -w .  pytango==$PYTANGO_VER

FROM $SRC_IMAGE

ARG BUILD_DATE
ARG GIT_REV
ARG DOCKER_TAG="0.1"

LABEL \
    org.opencontainers.image.title="alpine-pytango" \
    org.opencontainers.image.description="PyTango on Alpine" \
    org.opencontainers.image.authors="GP Orcullo<kinsamanka@gmail.com>" \
    org.opencontainers.image.version=$DOCKER_TAG \
    org.opencontainers.image.url="https://hub.docker.com/repository/docker/kinsamanka/alpine-pytango" \
    org.opencontainers.image.source="https://gitlab.com/kinsa13/tango/Docker/alpine-pytango" \
    org.opencontainers.image.revision=$GIT_REV \
    org.opencontainers.image.created=$BUILD_DATE

COPY --from=src /pytango*whl /tmp

USER root
RUN apk  --update add --no-cache boost-python3 py3-numpy py3-pip && \
    pip install /tmp/*whl && \
    rm /tmp/*

USER tango
